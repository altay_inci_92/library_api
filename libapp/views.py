from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from models import Author, Book
import csv
import json, datetime


@csrf_exempt
def library(request):
    if request.method == 'POST':
        Author.objects.all().delete()
        Book.objects.all().delete()

        lib_file = request.FILES.get('data')
        reader = csv.reader(lib_file, delimiter=' ', quotechar='|')
        data_file = []
        for row in reader:
            data_file.append(','.join(row))

        for element in data_file:
            element = element.split(',')
            leng = len(element)

            _books, x = Book.objects.get_or_create(title=element[0],
                                                   lc_classification=element[
                                                       1])
            start = 2
            for i in range(start, leng, 3):
                _authors, x = Author.objects.get_or_create(name=element[i],
                                                           surname=element[
                                                               i + 1],
                                                           dateofbirth=element[
                                                               i + 2]
                                                           )
                _books.author.add(_authors)
            _books.save()

        return HttpResponse('books and authors created successfully.')

    elif request.method == 'PATCH':
        data = []
        for line in request.body.splitlines():
            data.append(line)
        lenght = len(data)
        for i in range(4, lenght - 2):
            element = data[i]
            element = element.split(',')
            leng = len(element)
            _books, x = Book.objects.get_or_create(title=element[0],
                                                   lc_classification=element[
                                                       1])
            start = 2
            for i in range(start, leng, 3):
                _authors, x = Author.objects.get_or_create(name=element[i],
                                                           surname=element[
                                                               i + 1],
                                                           dateofbirth=element[
                                                               i + 2]
                                                           )
                _books.author.add(_authors)
            _books.save()

        return HttpResponse('books and authors created successfully.')

    else:
        return HttpResponse('Hatali metot.')


@csrf_exempt
def book(request):
    if request.method == 'GET':
        book_list = []
        ll=[]
        ky=7
        regex = request.GET.get('title')
        for book in Book.objects.filter(title__icontains=regex):
            example_book = {
                'book_id': book.id,
                'title': book.title,
                'lc_classification': book.lc_classification
            }
            authors = []
            for author in book.author.all():
                authors.append({
                    'author_id': author.id,
                    'name': author.name,
                    'surname': author.surname,
                    'dateofbirth': author.dateofbirth
                })
            example_book['authors'] = authors
            book_list.append(example_book)
        return HttpResponse(book_list)

    if request.method == 'POST':
        data = request.FILES.get('data')
        reader = csv.reader(data, delimiter=' ', quotechar='|')
        data_file = []
        for row in reader:
            data_file.append(','.join(row))

        new_data = data_file[0]
        new_data = new_data.split(',')
        leng = len(new_data)

        _book, x = Book.objects.get_or_create(title=new_data[0],
                                              lc_classification=new_data[1])
        start = 2
        for i in range(start, leng, 3):
            _author, x = Author.objects.get_or_create(name=new_data[i],
                                                      surname=new_data[i + 1],
                                                      dateofbirth=new_data[
                                                          i + 2]
                                                      )
            _book.author.add(_author)
        _book.save()
        return HttpResponse({'book created successfully!'})


@csrf_exempt
def author(request):
    if request.method == 'GET':
        authors = []
        regex = request.GET.get('surname')
        for author in Author.objects.filter(surname__icontains=regex):
            a = {
                'author_id': author.id,
                'name': author.name,
                'surname': author.surname,
                'dateofbirth': author.dateofbirth
            }
            book_list = []
            for book in author.books.all():
                book_list.append(
                    {
                        'book_id': book.id,
                        'title': book.title,
                        'lc_classification': book.lc_classification
                    }
                )
            a['books'] = book_list
            authors.append(a)
        return HttpResponse(authors)

    if request.method == 'POST':
        data = request.FILES.get('data')
        reader = csv.reader(data, delimiter=' ', quotechar='|')
        data_file = []
        for row in reader:
            data_file.append(','.join(row))

        for element in data_file:
            data = element.split(',')
            author, _ = Author.objects.get_or_create(
                name=data[0],
                surname=data[1],
                dateofbirth=data[2]
            )
        return HttpResponse({'author created successfully!'})


@csrf_exempt
def book_get(request, id=None):
    if request.method == 'GET':
        if id:
            if Book.objects.filter(id=id).exists():
                book = Book.objects.get(id=id)
                data = {
                    'book_id': book.id,
                    'title': book.title,
                    'lc_classification': book.lc_classification
                }
                authors = []
                for author in book.author.all():
                    authors.append({
                        'author_id': author.id,
                        'name': author.name,
                        'surname': author.surname,
                        'dateofbirth': str(author.dateofbirth)
                    })
                data['authors'] = authors
                return HttpResponse(json.dumps(data))
            return HttpResponse({'detail': 'There is not this book!'})
        else:
            return HttpResponse({'detail': 'There is not this book!'})


# This is first comment.
# This is second commit.
# This is third commit.
# This is fourth commit.
# This is fifth commit.
# This is sixth commit.


# This is Sevent commit.
# This is eight commit.
# This is nine commit.
# This is tenth commit.