from __future__ import unicode_literals

from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    dateofbirth = models.DateField(null=True)

    def __unicode__(self):
        return self.name + ' ' + self.surname


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ManyToManyField(Author, related_name='books')
    lc_classification = models.CharField(max_length=200)

    def __unicode__(self):
        return self.title
