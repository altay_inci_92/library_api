from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^author/$', views.author, name='author'),
    url(r'^book/(?P<id>[0-9]+)/$', views.book_get, name='book_get'),
    url(r'^book/$', views.book, name='book'),
    url(r'^library/$', views.library, name='library'),

]
